from __future__ import division
import numpy as np
import scipy as sp
from scipy.special import genlaguerre
from matplotlib import pyplot as plt

a = 5.29e-11
Tot = 7000

#Quantum numbers for which we will plot the distribution
n = int(raw_input("Please input principle quantum number \n \
(positive integer, recommended 10): "))
l = int(raw_input("Please input angular momentum quantum number: \n \
(positive integer less than n, recommend 2): "))


#Creates Laguerre Polynomials that we need for our given quatum numbers n, l
def Lag(n, l):
    L_x = genlaguerre(n-l-1,2*l+1)
    return L_x


dist_arr = range(Tot)
Rad_arr = np.zeros(Tot)

prob = np.zeros(Tot)
for i in range(Tot):
    dist_arr[i] = a*dist_arr[i]

#Returns the probability distrubition for different quantum numbers
def Radial(n,l,T):
    dist_arr = range(Tot)
    Rad_arr = np.zeros(Tot)

    prob = np.zeros(Tot)
    for i in range(Tot):
        dist_arr[i] = (6e-2)*a*dist_arr[i]

    for i in range(T-1):
        nu = Lag(n,l) #Laguerre polynomial for n, l
        Rad_1 = nu(2*dist_arr[i+1]/(a*n))
        exp = np.exp(-dist_arr[i+1]/(a*n)) #Exponential part of function
        rho_exp = (dist_arr[i+1]/(a*n))**(l+1)

        Rad_arr[i] = 1/(dist_arr[i+1])*(Rad_1)*exp*rho_exp

        prob[i] = (Rad_arr[i]**2)*dist_arr[i]**2
    return prob
    

title = 'Radial Probability, n=' + str(n) + ', l=' + str(l)
plt.title(title)
plt.xlabel("Radial Position")
plt.ylabel("Probability (Unnormailized)")
p2 = plt.plot(dist_arr, Radial(n,l,Tot), 'r')
plt.show()
    
    
