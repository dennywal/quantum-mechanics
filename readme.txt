# README #


This repository contains two python scripts.
One of the scripts is for calculating the probability distribution function for a quantum free particle over time, called gaussian_free_particle.
This script is animated to show the time evolution of the function.


The other script calculates the radial probability distribution function for the Hydrogen Atom with different principle and angular quantum numbers.
The inputs for this script are the quantum numbers n and l, which must take integer values, n > 0, l >= 0. With l < n.
These quantum numbers represent the different energy levels of a hydrogen atom and the resulting plot gives the relative probability of finding an electron at a given radius.

These scripts both require the numpy, scipy and matplotlib libraries.

This repository also contains a few still images created from these scripts.