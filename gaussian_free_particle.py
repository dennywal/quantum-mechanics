from __future__ import division
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation

pi = np.pi

width = 10 #width of graph
a = 1
h = 1
m = 1

interval = np.arange(-width, width, 0.001)

def psi2(x, t):
    w = np.sqrt(a/(1 + ((2*h*a*t)/m)**2))

    psi2 = np.sqrt(2/pi)*w*np.exp(-2*(w**2)*x**2)

    return psi2

fig = plt.figure()
ax = plt.axes(xlim=(-width, width), ylim = (0, 1))
line, = ax.plot([], [], lw=2)

def init():
    line.set_data([], [])
    return line,

def animate(t):
    x = np.linspace(-width, width, 1000)
    time = t/(30)
    y = psi2(x, time)
    line.set_data(x, y)
    return line,

anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=250, interval=20, blit=True)


plt.plot(interval, psi2(interval, 0))
plt.title("Probability Dist. of Free Particle over time")
plt.xlabel("Position")
plt.ylabel("Probability")
plt.show()
